import com.machine.services.SensorsServices;
import org.junit.Assert;
import org.junit.Test;

import java.util.PriorityQueue;
import java.util.Queue;

import static org.junit.Assert.*;

public class TestSensors {

    private SensorsServices sensorsServices = new SensorsServices();

    @Test
    public void getAverage() {
        String message = sensorsServices.getValueAverge(432, 32);
        assertEquals("valores promedios normales", message);

    }

    @Test
    public void getMaxAndMin() {
        String message = sensorsServices.getDataMinAndMax(43, 54);
        assertEquals("valores minimos y maximos normales", message);

    }

}
