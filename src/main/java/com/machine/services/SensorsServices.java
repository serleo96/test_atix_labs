package com.machine.services;

import com.machine.utils.Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;


public class SensorsServices {

    public Map<String, String> sensor(Queue<Integer> queue) {

        Map<String, String> message = new HashMap<>();

        Integer prom = 0;
        Integer s = Constants.CONSTANT_S;
        Integer divisor = Integer.valueOf(queue.size());
        while (!queue.isEmpty()) {
            Integer a = queue.poll();
            Integer b = queue.poll();
            prom = a + b + prom;
            message.put("max_and_min", getDataMinAndMax(a, b));
        }
        message.put("average", getValueAverge(prom, divisor));
        return message;
    }

    public String getDataMinAndMax(Integer a, Integer b) {
        Integer s = Constants.CONSTANT_S;

        if (!(getValueMaxn(a, b) - getValueMin(a, b) > s)) {
            return "valores minimos y maximos normales";
        } else {
            return "anomalia en parametros";
        }
    }


    private Integer getValueMin(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }

    private Integer getValueMaxn(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    public String getValueAverge(Integer prom, Integer divisor) {
        Integer m = Constants.CONSTANT_M;
        if (!((prom / divisor) < m)) {
            return "valores promedios normales";
        } else {
            return "anomalia en parametros";
        }
    }

}
