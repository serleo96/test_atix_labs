package com.machine.controller;

import com.machine.services.SensorsServices;
import spark.Request;
import spark.Response;

import java.util.PriorityQueue;
import java.util.Queue;

public class SensorsController {

    private SensorsServices sensorsServices = new SensorsServices();

    public Object monitorSensors(Request request, Response response) {

        Integer monitorA = Integer.valueOf(request.queryParams("monitor_A"));
        Integer monitorB = Integer.valueOf(request.queryParams("monitor_B"));
        Integer monitorC = Integer.valueOf(request.queryParams("monitor_C"));
        Integer monitorD = Integer.valueOf(request.queryParams("monitor_D"));

        Queue<Integer> monitorproces = new PriorityQueue<>();
        monitorproces.add(monitorA);
        monitorproces.add(monitorB);
        monitorproces.add(monitorC);
        monitorproces.add(monitorD);

        return sensorsServices.sensor(monitorproces);
    }
}
