package com.machine;

import com.google.gson.Gson;
import com.machine.controller.AppController;
import com.machine.controller.SensorsController;

import static spark.Spark.*;

public class Main {

    private AppController appController = new AppController();

    public static void main(String[] args) {
        get("/hello", (req, res) -> "Hello World");
        path("/maiche", () -> {
        });

        post("/monitor", (req, res) -> {
            res.type("application/json");
            return new Gson().toJson(new SensorsController().monitorSensors(req, res));
        });

        get("/ping", (request, response) -> {
            response.type("application/json");
            return new Gson().toJson(new AppController().pong(request, response));
        });
    }
}
